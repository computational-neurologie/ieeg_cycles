import sys
sys.path.insert(0, '/data/gpfs-1/users/kashyapa_c/work')
print(sys.path)

import iEEG_STC
import iEEG_STC.reader as reader
from iEEG_STC.reader import ECOGReader
import pandas as pd
import numpy as np
from scipy import stats
import mne
import os
from scipy import signal
import tqdm
from tqdm import tqdm, tqdm_notebook
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import specgram
from torchvision import datasets, models, transforms
import torch
torch.nn.Module.dump_patches = False
import seaborn as sns
from datetime import datetime
from time import perf_counter
import warnings
import statistics
warnings.filterwarnings('ignore')
t0 = perf_counter()
from operator import add
# ## Initiate paths



import matplotlib
matplotlib.use('Agg')
# +
parent_path = '/data/gpfs-1/users/kashyapa_c/work'
meta_path = f'{parent_path}/meta_files'
pat = 'pat565'
data_path =  f'/data/gpfs-1/groups/ag_meisel/work/epilepsiae_bids/pat_565/EEG'

#pat = sys.argv[1]
# data_file = '95800102_0005.data'
#data_file = sys.argv[2].split('/')[-1]

#data_f = '95800102_0027.data'
data_f = str(sys.argv[1])

data_idnum = data_f.split('.')[0]


data_file = f'{data_path}/{data_f}'
print(f'Starting {data_file}')
####### Data now read in, next step: identification of peaks
eegdir =  f'{parent_path}/spiking_output' #ToDo: specify directory in which images and files shall be saved

##path_to_quality = "../Epilepsiae/pat958/meta/pat958_segments_goodness.csv"
#elecs = pd.read_csv("../Epilepsiae/pat958/meta/pat958_electrodes.csv")
#pre_snr_filename = "./pre_snr.csv" #pre snr calculation results, for potentially checking other window sized
proj_dir =  f'{parent_path}/critical_spiking'
imgs = f'/data/gpfs-1/groups/ag_meisel/scratch/SPECTS/{data_idnum}' # dir with IED / NONIED image dirs (name of subdir)
os.makedirs(imgs, exist_ok=True)
snr_out_dir = f"{parent_path}/spike_output"
spectdir =  f"{imgs}/IEDS/"  ################################################################## CHANGE: dir here
os.makedirs(spectdir, exist_ok=True)
#
model_dir = f"{parent_path}/critical_spiking/" #directory in which model_aied.pt is located
 #directory in which imagefolder is
os.makedirs(f"{parent_path}/spiking_output/{pat}", exist_ok=True)

#path to file containing information about interictal events, e.g. sharp waves
#interictal_event_file = "../Epilepsiae/pat958/meta/pat958_interictal_events.csv" #changepath
interictal_event_file = f'{meta_path}/{pat}_interictal_events.csv'

## following two parameters only necessary if we use other calculation of "good electrodes"
#path to file containing electrode positions of all electrodes - required for caluclating distance of distinct electrodes
# electrode_position_file = "../Epilepsiae/pat958/meta/pat958_electrodes.csv"
electrode_position_file = f'{meta_path}/{pat}_electrodes.csv'
#maximal distance electrodes are allowed to be apart from each other
maxdist_electrodeneighbors = 10
#freqlist = pd.read_csv(f'/data/gpfs-1/users/muellepm_c/work/LRTC_project/{pat}/meta/psd_0.125_full_length_5.0.config',
#                      sep='\s*=\s*',index_col=0).loc['notch_filter']

freqlist = []
# -

# ### Check if outfile already exists
# If so immediately exit the script

# +
#if os.path.isfile(f"{snr_out_dir}/{pat}/{data_file.split('.')[0]}_snr.csv"):
#    print('Outfile already exists -> Skipping')
#    sys.exit()
# -

# ## Preprocess data
# Filtering, channel extraction

def create_csv_preprocessing(pat,data_file ,meta_path, data_path, use_only_these_channels=None):

    data_object = ECOGReader(f'{data_file}')
    data_object.read_data()
#    path_to_quality = f"{meta_path}/{pat}_segments_goodness.csv"
#    test = pd.read_csv(path_to_quality, index_col=0)
#    quality = pd.read_csv(path_to_quality, index_col=0).loc[data_object.head_dict['recordingID']]
#    to_delete_ch = np.array(data_object.head_dict['elec_names'])[~np.in1d(data_object.head_dict['elec_names'],
#                                                                   quality.index[quality.values.astype(bool)])]
#    data_object.delete_channels(to_delete_ch)
    if not use_only_these_channels is None:
        data_object.delete_channels(np.array(data_object.head_dict['elec_names'])[~np.in1d(data_object.head_dict['elec_names'],
                                                                                          use_only_these_channels)])
    # Extract electrodes
    elecs = pd.read_csv(f"{meta_path}/{pat}_electrodes.csv")
    # Delete all non ecog electrodes, i.e., electrodes without coordinates
    elecs = elecs.loc[elecs['x'] != '-']
    data_object.delete_channels(data_object.data.columns[~np.in1d(data_object.data.columns, elecs)])

    data = data_object.data.drop(['Time'], axis = 1)
    sfreq = data_object.head_dict['sample_freq']
    if data.shape[1] == 0:
        return None
    info = mne.create_info(ch_names=data.columns.tolist(), sfreq=sfreq)
    raw = mne.io.RawArray(data.T, info)
    raw.set_channel_types({ch: 'eeg' for ch in raw.ch_names})
    #mne.export.export_raw(outname, raw, fmt='edf')
    return raw


### 2. CLEAN DATA:
def cleaner(raw, freqlist):
    """
    iEEG PREPROCESSING PIPELINE
    INPUT: RAW iEEG (MNE)
    OUTPUT: CLEANED iEEG ('picks')
    # note: resampling should already be based on a filtered signal!
    # (i.e., first filtering, then down sampling)
    """
    ### 1. rereference data (average rereference)
    raw.set_eeg_reference('average', projection=True)
    # raw.plot_psd(area_mode='range', tmax=10.0) # visual verification
    print('Original sampling rate:', raw.info['sfreq'], 'Hz')

    ### 2. notch filter
    raw = raw.notch_filter(np.arange(50, int(raw.info['sfreq'] / 2) - 1, 50), filter_length='auto',
                           phase='zero')  # 60, 241, 60
    #add notch filter for variable frquencies - take mean of always two values
    freqlist_notch = []
    for i in range(int(len(freqlist)/2)):
        freqlist_notch.append(statistics.mean([freqlist[i*2],freqlist[i*2+1]]))
    if(len(freqlist_notch)>=1): #check that there is frequency in list
        raw = raw.notch_filter(freqlist_notch, filter_length='auto', phase='zero')
    # raw.plot_psd(tmin=tmin, tmax=tmax, fmin=fmin, fmax=fmax, n_fft=n_fft,
    #              n_jobs=1, proj=True, ax=ax, color=(1, 0, 0), picks=picks) # visual verification

    ### 3. other filters
    # low pass filter (250Hz)
    raw = raw.filter(None, 250. if 250 < (int(raw.info['sfreq'] / 2)-1) else 125., h_trans_bandwidth='auto', filter_length='auto', phase='zero')
    # high pass filter (1Hz) - remove slow drifts
    raw = raw.filter(1., None, l_trans_bandwidth='auto', filter_length='auto', phase='zero')
    # raw.plot_psd(area_mode='range', tmax=10.0) # visual verification

    ### 4. downsampling (200Hz)
    raw = raw.resample(200, npad='auto')
    print('New sampling rate:', raw.info['sfreq'], 'Hz')

    ### 5. reject bad channels
    def check_bads_adaptive(raw, picks, fun=np.var, thresh=3, max_iter=np.inf):
        ch_x = fun(raw[picks, :][0], axis=-1)
        my_mask = np.zeros(len(ch_x), dtype=bool)
        i_iter = 0
        while i_iter < max_iter:
            ch_x = np.ma.masked_array(ch_x, my_mask)
            this_z = stats.zscore(ch_x)
            local_bad = np.abs(this_z) > thresh
            my_mask = np.max([my_mask, local_bad], 0)
            print('iteration %i : total bads: %i' % (i_iter, sum(my_mask)))
            if not np.any(local_bad):
                break
            i_iter += 1
        bad_chs = [raw.ch_names[i] for i in np.where(ch_x.mask)[0]]
        return (bad_chs)

    # Find the first index of the super-bad channels
    '''endIndex = 1
    for i, name in enumerate(
            raw.info['ch_names']):  # can add new logic to reject other channels that are definitely bad
        if len(re.compile(r'C\d{3}').findall(name)) > 0:
            endIndex = i
            break

    bad_chs = raw.ch_names[endIndex:]
    bad_chs.extend(check_bads_adaptive(raw, list(range(0, endIndex)), thresh=3))
    raw.info['bads'] = bad_chs'''
    #fix from Paul
    raw.info['bads'] = check_bads_adaptive(raw, list(range(0, len(raw.ch_names))), thresh=3)

    #     print(bad_chs)
    #     print(len(raw.info['bads'])) # check which channels are marked as bad
    ### PICK ONLY GOOD CHANNELS:
    picks = raw.pick_types(eeg=True, meg=False, exclude='bads')
    print("NUMBER OF CHANNELS FOR SUBJECT {}: {}".format(subject, len(picks.info['chs'])))
    #     print("THIS SHOULD BE 0: {}".format(len(picks.info['bads'])) ) # check statement


    return (picks)


# +
#takes in two excels, one containing electrode positions and one containing interictal event patterns
def find_bestchans_positionwise(interictal_event_file, electrode_position_file, maxdist_elec):
    #check for channels having sharp waves
    bestchans_read = pd.read_csv(interictal_event_file)
 #   print(bestchans_read)
#    bestchans_read = bestchans_read[bestchans_read['pattern'] == "sharp waves"]
    #print(bestchans_read)
    #read in electrode positions
    electrode_positions = pd.read_csv(electrode_position_file)

    #turn wave positions into list of absolute numbers
    sharp_waves = [i for i, e in enumerate(electrode_positions['name']) if e in bestchans_read['electrode'].values.tolist()]

    sharp_wave_coords = electrode_positions.iloc[sharp_waves]
    sharp_wave_coords = sharp_wave_coords.reset_index()

    chan_list = [] #list saving all
    dist_list = [] #only for plotting all distances

    #calculate which electrodes are close to initial "good" electrodes
    for i in range(len(electrode_positions)):
        for j in range(len(sharp_wave_coords)):
            p1 = np.array([electrode_positions['x'][i], electrode_positions['y'][i], electrode_positions['z'][i]])
            p2 = np.array([sharp_wave_coords['x'][j], sharp_wave_coords['y'][j], sharp_wave_coords['z'][j]])
            squared_dist = np.sum((p1-p2)**2, axis=0)
            dist = np.sqrt(squared_dist)
            dist_list.append(dist)
            if(dist < maxdist_elec):
                chan_list.append(i)

    plt.hist(dist_list)

    close_good_chans = electrode_positions['name'][chan_list]

    #check that found channels were initially not considered as bad channels - in segment goodnes excel table
    final_chans = np.intersect1d(data.index.values, close_good_chans.values)

    #create list with absolute positions for further calculation
    bestchans = [i for i, e in enumerate(data.index.values) if e in final_chans]
    print(bestchans)
    return bestchans



#calcualte best channels and near neighbors
#bestchans = find_bestchans_positionwise(interictal_event_file, electrode_position_file, maxdist_electrodeneighbors)
#add neighbors to "good" channels with sharp waves, only considering the order of electrodes on brain
def find_bestchans_numberwise(interictal_event_file):
    #check for channels having sharp waves
    bestchans_read = pd.read_csv(interictal_event_file)
  #  bestchans_read = bestchans_read[bestchans_read['pattern'] == "sharp waves"]['electrode']
    bestchans_read= bestchans_read['electrode']
    bestchanlist = []
    for i in bestchans_read:
        bestchanlist.append(i)
        n = i[:len(i)-1] #get the letters of the electrode
        if(i[len(i)-1] == '0'): #if on left outer side, only add right neighbor (no left present)
            bestchanlist.append(n + str(int(i[len(i)-1])+1))
        else:
            if(i[len(i)-1] == '8'): #if on the right outer side, only add left neighbor (no right present)
                bestchanlist.append(n + str(int(i[len(i)-1])-1))
            else: #if in middle: add left and right neighbor
                bestchanlist.append(n + str(int(i[len(i)-1])-1))
                bestchanlist.append(n + str(int(i[len(i)-1])+1))
    return bestchanlist

bestchans = find_bestchans_numberwise(interictal_event_file)
#print('before')
print(bestchans)
print(len(bestchans))
#print('after')
#fail = superfail

# +
t1=perf_counter()
subject = pat
raw = create_csv_preprocessing(pat, data_file, meta_path, data_path,
                               use_only_these_channels=bestchans)
if raw is None:
    print('No good IED channels in file. -> Script exited')
    sys.exit()
picks = cleaner(raw, freqlist)

data = pd.DataFrame(picks.get_data().T, columns = picks.ch_names)

### quick check: transpose if not in proper format (rows = chans, cols = timepoints) - build on this later.
if len(data) > len(data.columns):
    data = data.T
    if type(data[0][0]) == str:
        data = data.drop(data.columns[0], axis=1)
    data = data.astype(float)
    print('CHECK: Number of channels ~ %d' % len(data))
else:
    data = data.astype(float)

print(f'Data cleaned {perf_counter()-t1:.0f}/{perf_counter()-t0:.0f} sec.')
# -

# #### Clean old data paths
# AUTO DUMP IED IMAGES: clears dir containing spectrograms if produced in previous iteration

os.makedirs(spectdir, exist_ok=True)
for filename in os.listdir(spectdir):
    file_path = os.path.join(spectdir, filename)
    try:
        if os.path.isfile(file_path) or os.path.islink(file_path):
            os.unlink(file_path)
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)
    except Exception as e:
        print('Failed to delete %s. Reason: %s' % (file_path, e))
print(spectdir)


# ### 2. LOAD TEMPLATE-MATCHING DETECTOR FUNCTIONS:

# +
def detect_peaks(x, mph=None, mpd=1, threshold=0, edge='rising',
                kpsh=False, valley=False, show=False, ax=None):
    """Detect peaks in data based on their amplitude and other features."""
    x = np.atleast_1d(x).astype('float64')
    if x.size < 3:
        return np.array([], dtype=int)
    if valley:
        x = -x
    # find indices of all peaks
    dx = x[1:] - x[:-1]
    indnan = np.where(np.isnan(x))[0]
    if indnan.size:
        x[indnan] = np.inf
        dx[np.where(np.isnan(dx))[0]] = np.inf
    ine, ire, ife = np.array([[], [], []], dtype=int)
    if not edge:
        ine = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) > 0))[0]
    else:
        if edge.lower() in ['rising', 'both']:
            ire = np.where((np.hstack((dx, 0)) <= 0) & (np.hstack((0, dx)) > 0))[0]
        if edge.lower() in ['falling', 'both']:
            ife = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) >= 0))[0]
    ind = np.unique(np.hstack((ine, ire, ife)))
    if ind.size and indnan.size:
        # NaN's and values close to NaN's cannot be peaks
        ind = ind[np.in1d(ind, np.unique(np.hstack((indnan, indnan-1, indnan+1))), invert=True)]
    # first and last values of x cannot be peaks
    if ind.size and ind[0] == 0:
        ind = ind[1:]
    if ind.size and ind[-1] == x.size-1:
        ind = ind[:-1]
    # remove peaks < minimum peak height
    if ind.size and mph is not None:
        ind = ind[x[ind] >= mph]
    # remove peaks - neighbors < threshold
    if ind.size and threshold > 0:
        dx = np.min(np.vstack([x[ind]-x[ind-1], x[ind]-x[ind+1]]), axis=0)
        ind = np.delete(ind, np.where(dx < threshold)[0])
    # detect small peaks closer than minimum peak distance
    if ind.size and mpd > 1:
        ind = ind[np.argsort(x[ind])][::-1]  # sort ind by peak height
        idel = np.zeros(ind.size, dtype=bool)
        for i in range(ind.size):
            if not idel[i]:
                # keep peaks with the same height if kpsh is True
                idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd) \
                    & (x[ind[i]] > x[ind] if kpsh else True)
                idel[i] = 0  # Keep current peak
        # remove the small peaks and sort back the indices by their occurrence
        ind = np.sort(ind[~idel])

    if show:
        if indnan.size:
            x[indnan] = np.nan
        if valley:
            x = -x
        _plot(x, mph, mpd, threshold, edge, valley, ax, ind)
    return ind

def _plot(x, mph, mpd, threshold, edge, valley, ax, ind):
    """Plot results of the detect_peaks function, see its help."""
    try:
        import matplotlib.pyplot as plt
    except ImportError:
        print('matplotlib is not available.')
    else:
        if ax is None:
            _, ax = plt.subplots(1, 1, figsize=(8, 4))

        ax.plot(x, 'b', lw=1)
        if ind.size:
            label = 'valley' if valley else 'peak'
            label = label + 's' if ind.size > 1 else label
            ax.plot(ind, x[ind], '+', mfc=None, mec='r', mew=2, ms=8,
                    label='%d %s' % (ind.size, label))
            ax.legend(loc='best', framealpha=.5, numpoints=1)
        ax.set_xlim(-.02*x.size, x.size*1.02-1)
        ymin, ymax = x[np.isfinite(x)].min(), x[np.isfinite(x)].max()
        yrange = ymax - ymin if ymax > ymin else 1
        ax.set_ylim(ymin - 0.1*yrange, ymax + 0.1*yrange)
        ax.set_xlabel('Data #', fontsize=14)
        ax.set_ylabel('Amplitude', fontsize=14)
        mode = 'Valley detection' if valley else 'Peak detection'
        ax.set_title("%s (mph=%s, mpd=%d, threshold=%s, edge='%s')"
                    % (mode, str(mph), mpd, str(threshold), edge))
        # plt.grid()
        plt.show()

def locate_downsample_freq(sample_freq, min_freq=200, max_freq=340):
    min_up_factor = np.inf
    best_candidate_freq = None
    for candidate in range(min_freq, max_freq+1):
        down_samp_rate = sample_freq / float(candidate)
        down_factor, up_factor = down_samp_rate.as_integer_ratio()
        if up_factor <= min_up_factor:
            min_up_factor = up_factor
            best_candidate_freq = candidate
    return best_candidate_freq


def butter_bandpass(low_limit, high_limit, samp_freq, order=5):
    nyquist_limit = samp_freq / 2
    low_prop = low_limit / nyquist_limit
    high_prop = high_limit / nyquist_limit
    b, a = signal.butter(order, [low_prop, high_prop], btype='band')
    def bb_filter(data):
        return signal.filtfilt(b, a, data)
    return bb_filter


def detect(channel, samp_freq, return_eeg=False, temp_func=None, signal_func=None):
    # assume that eeg is [channels x samples]
    # Round samp_freq to the nearest integer if it is large
    if samp_freq > 100:
        samp_freq = int(np.round(samp_freq))
    down_samp_freq = locate_downsample_freq(samp_freq)
    template = signal.triang(np.round(down_samp_freq * 0.06))
    kernel = np.array([-2, -1, 1, 2]) / float(8)
    template = np.convolve(kernel, np.convolve(template, kernel, 'valid') ,'full')
    if temp_func:
        template = temp_func(template, samp_freq)
    if signal_func:
        channel = signal_func(channel, samp_freq)

    down_samp_rate = samp_freq / float(down_samp_freq)
    down_samp_factor, up_samp_factor = down_samp_rate.as_integer_ratio()
    channel = signal.detrend(channel, type='constant')
    results = template_match(channel, template, down_samp_freq)
    up_samp_results = [np.round(spikes * down_samp_factor / float(up_samp_factor)).astype(int) for spikes in results]
    if return_eeg:
        return up_samp_results, [channel[start:end] for start, end in results]
    else:
        return up_samp_results

def template_match(channel, template, down_samp_freq, thresh=7, min_spacing=0): #######@@@############################## CHANGE: d:7,0
    template_len = len(template)
    cross_corr = np.convolve(channel, template, 'valid')
    cross_corr_std = med_std(cross_corr, down_samp_freq)
    detections = []
    # catch empty channels
    if cross_corr_std > 0:
        # normalize the cross-correlation
        cross_corr_norm = ((cross_corr - np.mean(cross_corr)) / cross_corr_std)
        cross_corr_norm[1] = 0
        cross_corr_norm[-1] = 0
        # find regions with high cross-corr
        if np.any(abs(cross_corr_norm > thresh)):
            peaks = detect_peaks(abs(cross_corr_norm), mph=thresh, mpd=template_len)
            peaks += int(np.ceil(template_len / 2.)) # center detection on template
            peaks = [peak for peak in peaks if peak > template_len and peak <= len(channel)-template_len]
            if peaks:
                # find peaks that are at least (min_spacing) secs away
                distant_peaks = np.diff(peaks) > min_spacing * down_samp_freq
                # always keep the first peak
                to_keep = np.insert(distant_peaks, 0, True)
                peaks = [peaks[x] for x in range(len(peaks)) if to_keep[x] == True]
                detections = [(peak-template_len, peak+template_len) for peak in peaks]
    return np.array(detections)

def med_std(signal, window_len):
    window = np.zeros(window_len) + (1 / float(window_len))
    std = np.sqrt(np.median(np.convolve(np.square(signal), window, 'valid') - np.square(np.convolve(signal, window, 'valid'))))
    return std


# -


# ### 3. RUN TEMPLATE-MATCHING DETECTOR:
#

# +
def autoDetect(eegdata, samp_freq = 200, subject = subject):
    """
    AUTODETECT: DETECTS ALL SPIKES IN EACH CHANNEL
         INPUT: raw eeg file (preprocessed signal)
        OUTPUT: all_detections (list containing a list of arrays for all detections),
                channel_names (eeg channel names corresponding to each detection list)
    """
    ### DETECT SPIKES:
    all_detections = []
    channel_names = []
    for i in range(eegdata.shape[0]):
        channel = eegdata.iloc[i,:].astype(float) # run on each row (chan)
        detections = detect(channel, samp_freq, return_eeg=False, temp_func=None, signal_func=None)
        all_detections.append(detections)
        channel_names.append(int(float((eegdata.columns[i]))))

    ### REFORMAT SPIKES:
    detections = pd.DataFrame(all_detections)
    channels = pd.DataFrame(channel_names)
    spikes = pd.concat([channels,detections], axis = 1)
    newspikes = spikes.transpose()
    newspikes.columns = newspikes.iloc[0]
    newspikes = newspikes.iloc[1:] # remove duplicate channel_name row
    ### AUTO LONG-FORMATTING OF SPIKES
    spikeDf = pd.DataFrame() # empty df to store final spikes and spikeTimes
    for idx, col in enumerate(newspikes.columns):
        # extract spikes for each column
        tempSpikes = newspikes.iloc[:,idx].dropna() # column corresponding to channel with all spikes
        tempSpikes2 = tempSpikes.tolist() # convert series to list
        # extract channel name for each spike (duplicate based on the number of spikes)
        tempName = tempSpikes.name # channel name
        tempName2 = [tempName] * len(tempSpikes) # repeat col name by the number of spikes in this channel
        tempDf = pd.DataFrame({'channel': tempName2, 'spikeTime': tempSpikes2})
        # save and append to final df
        spikeDf = pd.concat([spikeDf, tempDf])
        spikeDf['fs'] = samp_freq
        spikeDf['subject'] = subject
    return(spikeDf)

spikes = autoDetect(data) ### eegfile, Fs, sessionname; kleen_fs=200, preprocess_fs=200

print("SPIKES DETECTED (TEMP MATCH) = ", len(spikes))
print("")
print(spikes[:3])
# -
from joblib import Parallel, delayed


a = (np.array(spikes['channel'])).tolist()
ind = max(a,key=a.count)

print(np.shape(np.where(np.array(a) == ind))[1])
print(np.max(a))


# ### 4. GENERATE INPUT IMAGES FOR CNN:

# +
t1 = perf_counter()
def spectimgs(eegd, spike):
    """
    SPECTS: GENERATE SPECTS FOR CNN
        INPUT: 1) eegdata, 2) spikedf (df from automated template-matching spike detector)
        OUTPUT: spects within ./SPECTS/IEDS
    """
    
    samp_freq = 200
    #######################################
    pad = 1 # d:1 number of seconds for window
    dpi_setting = 300 # d:300
    Nfft = 128*(samp_freq/500) # d: 128
    h = 3
    w = 3
    #######################################
    try:
        subject = spike.subject.values[0]
        chan_name = int(spike.channel.values[0]) # zero idxed -1
        spikestart = spike.spikeTime.values[0][0] # start spike
        ### select eeg data row
        ecogclip = eegd
        ### filter out line noise
        b_notch, a_notch = signal.iirnotch(50.0, 30.0, samp_freq)
        ecogclip = pd.Series(signal.filtfilt(b_notch, a_notch, ecogclip))

           ### trim eeg clip based on cushion
            ### mean imputation if missing indices
        end = int(float((spikestart+int(float(pad*samp_freq)))))
        start = int(float((spikestart-int(float(pad*samp_freq)))))
        if end > max(ecogclip.index):
            temp = list(ecogclip[list(range(spikestart-int(float(pad*samp_freq)), max(ecogclip.index)))])
            cushend = [np.mean(ecogclip)]*(end - max(ecogclip.index))
            temp = np.array(temp + cushend)
        elif start < min(ecogclip.index):
            temp = list(ecogclip[list(range(min(ecogclip.index), spikestart+pad*samp_freq))])
            cushstart = [np.mean(ecogclip)]*(min(ecogclip.index)-start)
            temp = np.array(cushstart, temp)
        else:
            temp = np.array(ecogclip[list(range(spikestart-int(float(pad*samp_freq)),
                                         spikestart+int(float(pad*samp_freq))))])

            ### PLOT AND EXPORT:
        fig, ax = plt.subplots(figsize=(h,w))
        ax.specgram(temp, NFFT = int(Nfft), Fs = samp_freq, noverlap=int(Nfft/2), detrend = "linear", cmap = "YlOrRd")
        ax.axis("off")
        ax.set_xlim(0, pad*2)
        ax.set_ylim(0, 100)
        plt.savefig(spectdir+subject+"_"+str(spikestart)+"_"+str(chan_name)+".png", dpi = dpi_setting)
        plt.close(fig)
    except Exception as e:
        print(e)
        #print("ERROR with IED portion:", i)
        plt.close()
    

out = [Parallel(n_jobs=-1)(delayed(spectimgs)(data.iloc[int(spikes.channel.values[i])],spikes[i:i+1]) for i in range(len(spikes)))]
     



print('SPECTOGRAMS DONE')
print(f'Spects saved {perf_counter()-t1:.0f}/{perf_counter()-t0:.0f} sec.')
# -


# ### 5. ResNet-18 CNN DETECTOR:

# +
t1 = perf_counter()
data_transforms = {
    imgs: transforms.Compose([
        transforms.Resize(224),
        transforms.Pad(1, fill=0, padding_mode='constant'),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])}

class ImageFolderWithPaths(datasets.ImageFolder):
    """Custom dataset that includes image file paths.
    Extends torchvision.datasets.ImageFolder
    """
    # override the __getitem__ method. this is the method that dataloader calls
    def __getitem__(self, index):
        # this is what ImageFolder normally returns
        original_tuple = super(ImageFolderWithPaths, self).__getitem__(index)
        # the image file path
        path = self.imgs[index][0]
        # make a new tuple that includes original and the path
        tuple_with_path = (original_tuple + (path,))
        return (tuple_with_path)

image_datasets = {x: ImageFolderWithPaths(os.path.join(proj_dir, x),
                                          data_transforms[x]) for x in [imgs]}
dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=1, # use batch=1, shuffle=F
                                             shuffle=False, num_workers=0) for x in [imgs]}

class_names = image_datasets[imgs].classes
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# extract image paths
path_names = []
for images,labels,paths in dataloaders[imgs]:
    path_names.append(paths)
# convert list of paths to dataframe col
df = pd.DataFrame(path_names)
df.columns = ['clip_ids']
df[['clip_ids','clip']] = df['clip_ids'].str.split("IEDS/",expand=True)
df['clip'] = df['clip'].str.rstrip('.png')
df[['subject','start','chan']] = df['clip'].str.split('_',expand=True)

##############################################

### B: LOAD PRETRAINED MODEL
try:
    model = torch.load(model_dir + 'model_aied.pt')
    # model.eval() # model architecture
except ImportError:
    print('TRAINED MODEL NOT FOUND: Check that trained model is in eegdir and name matches: model_aied.pt')

###############################################

### C: RUN MODEL
y_pred = []
with torch.no_grad():
    for inputs,labels,paths in dataloaders[imgs]:
        inputs = inputs.to(device)
        labels = labels.to(device)
        outputs = model.forward(inputs)
        _,predicted = torch.max(outputs, 1)
        pred = predicted.numpy()
        lab = labels.numpy()
        y_pred.append(pred)

# reformat outputs:
y_pred_flat = np.concatenate((y_pred),axis=0)
# classes = ['class 0', 'class 1']
df['predicted_class'] = y_pred_flat
# # export as .csv
#df.to_csv(proj_dir+'all_predictions_.csv', encoding='utf-8', index=False)
print(df[:3]) # here, 1 = nonied, 0 = ied (df)
print(f'Model run {perf_counter()-t1:.0f}/{perf_counter()-t0:.0f} sec.')


# -


#remove spikes that are too close to other spikes - returns list for that
# 1 means stay, 0 means remove
# in df: sort by location, then by channel ->
def spike_density_remover(testlist, dist):
    remove_list = []
    check = False
    for i in range(len(testlist)-1):
        if(testlist[i+1]-testlist[i]<dist and testlist[i+1]>=testlist[i]):
            remove_list.append(0)
            check = True
        else:
            if check:
                remove_list.append(0)
                check = False
            else:
                remove_list.append(1)
    #add number for last position
    if(check):
        remove_list.append(0)
    else:
        remove_list.append(1)
    return remove_list


# +
### 6-adjusted copy. CLEAN SPIKE DF FOR EXPORT:

# +
########## !!! saves single spike position for each channel !!! #############
t1 = perf_counter()
def dataCleaner(df, dist_max, dist_singlepeak, samp_freq = spikes.fs.values[0], win = 0.5):  #in our case samp_freq = 200
    """
    CLEANS SPIKE DATA FOR EXPORT
    INPUT: df from resnet model, win = number of seconds allowed for spike overlap 
                                (i.e., spikes within 3s of ea.other = single event))
    OUTPUT: clean df with subjectid, spikeStart, 
    channels (where spikes detected) - if contactName present, change to rownames, 
    numChannels (# channels spike detected)
    """
    ### only keep spikes: predicted_class = 0
    df = df[df.predicted_class == 0]
    if len(df) == 0:
        finaldf = pd.DataFrame(columns=['subject', 'spikeStart',
                            'channels', 'numChannels', 'spikeStart_sec', 'spikeStart_dt', 'exactPeaks', 'numPeaks']) #added exactPeaks
        return finaldf
    df['start'] = df['start'].astype(int) # convert from str to int
    df['chan'] = df['chan'].astype(int) # convert from str to int
    
    
    ### sort start times in df:
    df = df.sort_values(by = 'start', ascending = True)
    
    
    ### dedupe spikes by col and time but keep original spikes
    bins =  np.arange(min(df.start.values), max(df.start.values), samp_freq*win) #create bins based on length of recording and specified windowsize
    spikebins = np.digitize(df['start'], bins) #add spikes to bins - get indices to which values belongs
    cleandf = df.groupby(spikebins)['start'].describe() #get descriptice statistics over spikes in bins
    spikepos = df.groupby(spikebins)['start'].apply(lambda x: x.values.tolist())
    chanlist = df.groupby(spikebins)['chan'].apply(lambda x: x.values.tolist()) #group channels in bins  together
    exactPeaks_pre = list(zip(chanlist, spikepos)) #add all exact peaks to df
    exactPeaks = []
    peakCount = []
    for i in range(len(exactPeaks_pre)): #create list of tuples for final output
        exactPeaks.append(list(zip(exactPeaks_pre[i][0],exactPeaks_pre[i][1])))
        peakCount.append(len(exactPeaks_pre[i][0]))
    chanlist = [list(set(x)) for x in chanlist] #creates set from list -> removes duplicate values
    chancounts = [len(l) for l in chanlist] #count number of channels per bin
    meanspikestart = (cleandf['mean']).astype(int)
    subjectid = [subject]*len(meanspikestart)
    
    ### reformat into new df
    finaldf = pd.DataFrame({'subject': subjectid, 'spikeStart': meanspikestart, 
                            'channels': chanlist, 'numChannels': chancounts, 'exactPeaks': exactPeaks, 'numPeaks': peakCount})
    #finaldf['spikeStart_sec'] = finaldf['spikeStart']/raw_edf.info['sfreq']
    data_object = ECOGReader(f'{data_file}')
    finaldf['spikeStart_sec'] = finaldf['spikeStart']/200
    finaldf['spikeStart_dt'] = finaldf['spikeStart_sec']+datetime.timestamp(data_object.head_dict['start_ts'])
    
    ### reject spikes detected in >= 12 channels within time window
    finaldf = finaldf[finaldf.numChannels < 12]
    
    return (finaldf)

data_t = data.T #required for finding the exact Peak positions
dist_max = 50 #(50 = 0.25seconds) for adjusting maximum - window in which max value shall be searched
dist_singlepeak = 2 #in seconds = distance Peaks minimally can be close to each other without being deleted
finaldf = dataCleaner(df, dist_max, dist_singlepeak)



out_pathnew= '/data/gpfs-1/groups/ag_meisel/work/epilepsiae_bids/pat_565/PROCESSED/'
#df.to_csv('uncleaned.csv', encoding='utf-8', index=False)
finaldf.to_csv(f'{out_pathnew}/{pat}_{data_f}.csv', encoding='utf-8', index=False)
#np.savetxt('chans.txt', bestchans)

#print(bestchans)


### export as .csv
#finaldf = finaldf.reset_index(drop=True)
#finaldf.to_csv(proj_dir+subject+'all_predictions_cleaned_nomaxadjust_6_2.csv', encoding='utf-8', index=False) ############################ CHANGE: export name
#print(finaldf[:3])
#print("")
#print("FINAL SPIKES DETECTED = ", len(finaldf))
#print(f'Spikes cleaned {perf_counter()-t1:.0f}/{perf_counter()-t0:.0f} sec.')
# -



#fail = superfail



#os.makedirs(spectdir, exist_ok=True)
#for filename in os.listdir(spectdir):
#    file_path = os.path.join(spectdir, filename)
#    try:
#        if os.path.isfile(file_path) or os.path.islink(file_path):
#            os.unlink(file_path)
#        elif os.path.isdir(file_path):
#            shutil.rmtree(file_path)
#    except Exception as e:
#        print('Failed to delete %s. Reason: %s' % (file_path, e))
#print(spectdir)

# +


#print("Removed spike positions: " + str(sum(finaldf['numChannels'] <= 1)))
#print("Remaining spike positions: " + str(len(finaldf['numChannels']) - sum(finaldf['numChannels'] <= 1)))

#finaldf = finaldf[finaldf['numChannels'] > 1]
#finaldf = finaldf.reset_index(drop=True)
# -


#finaldf.head()


# +
#t1 = perf_counter()
#remove spikes if they are too close to another spike
#chanlist = []
#poslist = []
#spikeStart_list = []
#spike_sec_list = []
#freq=200

#for meanspikecount in range(len(finaldf["spikeStart"])): #iterate over all found spikes (mean)
#    for peakcount in range(finaldf["numPeaks"][meanspikecount]): #calculate snr for every peak
#        peak = finaldf["exactPeaks"][meanspikecount]
#        chanlist.append(peak[peakcount][0])
#        poslist.append(peak[peakcount][1])
#        spike_sec_list.append(finaldf["spikeStart_dt"][meanspikecount])
#        spikeStart_list.append(finaldf["spikeStart"][meanspikecount])

### reformat into new df
#finaldf_cp = pd.DataFrame({'channel': chanlist, 'position': poslist, 'spike_secs': spike_sec_list, 'spikeStart': spikeStart_list})
#print(finaldf_cp)

#finaldf_cp.sort_values(by=['channel', 'position'], inplace=True)
#remove_spike_list = spike_density_remover(finaldf_cp['position'].to_list(), dist_singlepeak*freq)
#print("Number of peaks too close to each other: ", remove_spike_list.count(1))
#print("Remaining Spikes: ", remove_spike_list.count(0))
#rm_spike_list = list(map(bool, remove_spike_list))
#finaldf_cp = finaldf_cp[rm_spike_list]
#finaldf_cp = finaldf_cp.reset_index(drop=True)

#finaldf_cp['chan_names'] = finaldf_cp['channel'].map(lambda x: bestchans[x]) #add actual channel names to csv


# +
#def calc_mean_spikes(spikedf, lenwindow):
#    lenwindow *= 200 #convert seconds into timesteps
#    res = []

#    for idx in range(len(finaldf_cp["position"])): #iterate over all found spikes (mean)
#        ts_spike = []
#        chan = spikedf['channel'][idx]
#        startpos = spikedf['position'][idx] - lenwindow
#        endpos = spikedf['position'][idx] + lenwindow
#        ts_spike = data.iloc[chan, startpos:endpos]
#        res.append(ts_spike.tolist())

#    return res

#lenwindow = 2 #seconds
#finaldf_cp['ts_window'] = calc_mean_spikes(finaldf_cp, lenwindow)

# -


#print(f"{snr_out_dir}/{pat}/{data_f.split('.')[0]}_good_spikes.csv")
#finaldf_cp.to_csv(f"{snr_out_dir}/{pat}/{data_f.split('.')[0]}_good_spikes.csv", encoding='utf-8', index=False) ############################ CHANGE: export name
#finaldf_cp.head()
#print(f'Good spikes extracted run {perf_counter()-t1:.0f}/{perf_counter()-t0:.0f} sec.')


#def calc_snr(stim_time, channel, window_start, window_end, bstart, bend, fs):
#    """
#    Parameters:
#        ccep: array (n_times)
#        stim_time: int, stimulus time relative to the start of the epoch, in ms
#        window_start: int, start of post stimulation window used to calculate variance, in ms
#        window_end: int, end of post stimulation window used to calculate variance, in ms
#        fs: int, sampling frequency
#    Returns:
#        SNR: int
#    """
#    conv = fs/1000
#    channames = data.index
#    window_start = int((window_start*conv + stim_time)) # convert to data points
#    window_end = int((window_end*conv + stim_time))  # convert to data points
#    baseline_start = int((stim_time - bstart*conv)) # baseline window starts 500 ms before stimulus
#    baseline_end = int((stim_time - bend*conv)) # baseline window ends 20 ms before stimulus

#    baseline_std = np.std(data_t[channames[channel]][baseline_start : baseline_end])
#    baseline_variance = baseline_std **2
#    window_std = np.std(data_t[channames[channel]][window_start: window_end])
#    window_variance = window_std **2
#    snr = window_variance/baseline_variance
#    return snr


# ### calculate snr for all spikes in all channels

# +
#def SNR_calc_complete(finaldf_cp, numwindows, stepsize, bstart, init_base_dist):
#    #load channels:
#    channames = data_t.index
#    freq = 200
#    conv = freq/1000 #conversion from ms in tp

#    wlength = int(400*conv) #in ms - required for finding maximum

#    snrvals = []

#    for meanspikecount in range(len(finaldf_cp["spikeStart"])): #iterate over all found spikes (mean)
#        res = []
#        for i in range(numwindows):
#            res.append(calc_snr(finaldf_cp['position'][meanspikecount], channames[finaldf_cp['channel'][meanspikecount]], i*stepsize-bstart, i*stepsize-init_base_dist, bstart, init_base_dist, freq))
#        snrvals.append(res)

#    finaldf_cp['SNRList'] = snrvals

#    finaldf_cp = finaldf_cp.reset_index(drop=True)
#    return finaldf_cp

#t1 = perf_counter()

#params for windowshifting
#numwindows = 150
#stepsize = 30 #in milliseconds
#bstart = 1500 #in milliseconds
#init_base_dist = 1200 #initial distance of baseline to spike, given int is distance in ms

#data = pd.read_csv("data6_eegdata_goodbads.csv")
#finaldf = finaldf.reset_index()
#snr_out_df = SNR_calc_complete(finaldf_cp, numwindows, stepsize, bstart, init_base_dist)
#snr_out_df.to_csv(f"{snr_out_dir}/{pat}/{data_file.split('.')[0]}_snr.csv", index=False)
#print(f'SNR saved {perf_counter()-t1:.0f}/{perf_counter()-t0:.0f} sec.')
# -


#snr_out_df.head()


# +
#calculate multiple welch value
#def calc_psd():
#    fs = 200

#    for i in range(0,5):
#        welch_val_out = pd.DataFrame()
        #for j in range(10): #calculate for all spikes and add together
#        for j in range(len(snr_out_df['position'])): #calculate for all spikes and add together
#            start_t = snr_out_df['position'][j].item() + i*200 -500
#            end_t = snr_out_df['position'][j].item() + i*200 -300
#            if(start_t<0):
#                continue
#            if(end_t>len(data.T)):
#                continue
#            welch_val = psd_welch(data.T.iloc[start_t:end_t], fs, not_uniques_thres_frac=20)
#            if 'mean' in welch_val_out:
#                welch_val_out['mean'] = np.log(welch_val.mean(axis=1)) + welch_val_out['mean']
                #print(welch_val_out['mean'])
#            else:
#                welch_val_out['mean'] = np.log(welch_val.mean(axis=1))


        #plt.plot(welch_val_out.index, welch_val_out['mean']/10, label="window "+str(i))
#        plt.plot(welch_val_out.index, welch_val_out['mean']/len(snr_out_df['position']), label="window "+str(i))
#        snr_out_df['welch_mean_window_'+str(i)] = welch_val_out['mean']
#    plt.legend(loc="upper right")
#    plt.show()
#    return


#calc_psd()
# -


# def plot_snr():
#     outlist = []
#    
#     #for i in range(len(snr_out_df['SNRList'][0])):
#     #    outlist.append(np.nanmean(snr_out_df['SNRList'].str[i]))
#     
#     sns.lineplot(data=list(filter(lambda x: x[1], snr_out_df['SNRList'])))
#     plt.xlabel("Time")
#     plt.ylabel("SNR")
#     plt.title("Mean values of all SNRs from all peaks")
#     
#     plt.axhline(y=np.nanmean(outlist), color='g')
#     plt.show()
#
#
# #comment out for server usage
# # plot_snr()

