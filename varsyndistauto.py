import sys
sys.path.insert(0, '/data/gpfs-1/users/kashyapa_c/work')


import iEEG_STC.utils as utils
import iEEG_STC.reader as reader
from iEEG_STC.reader import ECOGReader
import pandas as pd
import numpy as np
from scipy import stats
import mne
import os
from scipy import signal
import tqdm
from tqdm import tqdm, tqdm_notebook
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import specgram
from torchvision import datasets, models, transforms
import torch
torch.nn.Module.dump_patches = False
import seaborn as sns
from datetime import datetime
from time import perf_counter
import warnings
import statistics
warnings.filterwarnings('ignore')
t0 = perf_counter()
from operator import add
# ## Initiate paths



import matplotlib
matplotlib.use('Agg')
# +
parent_path = '/data/gpfs-1/users/kashyapa_c/work'

#idnum = 958
pat = str(sys.argv[1])

data_path =  f'/data/gpfs-1/groups/ag_meisel/work/epilepsiae_bids/{pat}/EEG'
meta_path = f'/data/gpfs-1/groups/ag_meisel/work/epilepsiae_bids/{pat}/META'
#pat = sys.argv[1]
# data_file = '95800102_0005.data'
#data_file = sys.argv[2].split('/')[-1]

data_f = str(sys.argv[2])

#print(str(sys.argv[0]))
#print(data_f)

data_idnum = data_f.split('.')[0]


data_file = f'{data_path}/{data_f}'
print(f'Starting {data_file}')
####### Data now read in, next step: identification of peaks
eegdir =  f'{parent_path}/spiking_output' #ToDo: specify directory in which images and files shall be saved

##path_to_quality = "../Epilepsiae/pat958/meta/pat958_segments_goodness.csv"
#elecs = pd.read_csv("../Epilepsiae/pat958/meta/pat958_electrodes.csv")
#pre_snr_filename = "./pre_snr.csv" #pre snr calculation results, for potentially checking other window sized
proj_dir =  f'{parent_path}/critical_spiking'
imgs = f'/data/gpfs-1/groups/ag_meisel/scratch/SPECTS/{data_idnum}' # dir with IED / NONIED image dirs (name of subdir)
os.makedirs(imgs, exist_ok=True)
snr_out_dir = f"{parent_path}/spike_output"
spectdir =  f"{imgs}/IEDS/"  ################################################################## CHANGE: dir here
os.makedirs(spectdir, exist_ok=True)
#
model_dir = f"{parent_path}/critical_spiking/" #directory in which model_aied.pt is located
 #directory in which imagefolder is
os.makedirs(f"{parent_path}/spiking_output/{pat}", exist_ok=True)

#path to file containing information about interictal events, e.g. sharp waves
#interictal_event_file = "../Epilepsiae/pat958/meta/pat958_interictal_events.csv" #changepath
interictal_event_file = f'{meta_path}/{pat}_interictal_events.csv'

## following two parameters only necessary if we use other calculation of "good electrodes"
#path to file containing electrode positions of all electrodes - required for caluclating distance of distinct electrodes
# electrode_position_file = "../Epilepsiae/pat958/meta/pat958_electrodes.csv"
electrode_position_file = f'{meta_path}/{pat}_electrodes.csv'
#maximal distance electrodes are allowed to be apart from each other
maxdist_electrodeneighbors = 10
#freqlist = pd.read_csv(f'/data/gpfs-1/users/muellepm_c/work/LRTC_project/{pat}/meta/psd_0.125_full_length_5.0.config',
#                      sep='\s*=\s*',index_col=0).loc['notch_filter']

freqlist = []
# -

# ### Check if outfile already exists
# If so immediately exit the script

# +
#if os.path.isfile(f"{snr_out_dir}/{pat}/{data_file.split('.')[0]}_snr.csv"):
#    print('Outfile already exists -> Skipping')
#    sys.exit()
# -

# ## Preprocess data
# Filtering, channel extraction

def create_csv_preprocessing(pat,data_file ,meta_path, data_path, use_only_these_channels=None):

    data_object = ECOGReader(f'{data_file}')
    data_object.read_data()
#    path_to_quality = f"{meta_path}/{pat}_segments_goodness.csv"
#    test = pd.read_csv(path_to_quality, index_col=0)
#    quality = pd.read_csv(path_to_quality, index_col=0).loc[data_object.head_dict['recordingID']]
#    to_delete_ch = np.array(data_object.head_dict['elec_names'])[~np.in1d(data_object.head_dict['elec_names'],
#                                                                   quality.index[quality.values.astype(bool)])]
#    data_object.delete_channels(to_delete_ch)
    if not use_only_these_channels is None:
        data_object.delete_channels(np.array(data_object.head_dict['elec_names'])[~np.in1d(data_object.head_dict['elec_names'],
                                                                                          use_only_these_channels)])
    # Extract electrodes
    elecs = pd.read_csv(f"{meta_path}/{pat}_electrodes.csv")
    # Delete all non ecog electrodes, i.e., electrodes without coordinates
    elecs = elecs.loc[elecs['x'] != '-']
    data_object.delete_channels(data_object.data.columns[~np.in1d(data_object.data.columns, elecs)])

    data = data_object.data.drop(['Time'], axis = 1)
    sfreq = data_object.head_dict['sample_freq']
    if data.shape[1] == 0:
        return None
    info = mne.create_info(ch_names=data.columns.tolist(), sfreq=sfreq)
    raw = mne.io.RawArray(data.T, info)
    raw.set_channel_types({ch: 'eeg' for ch in raw.ch_names})
    #mne.export.export_raw(outname, raw, fmt='edf')
    return raw


### 2. CLEAN DATA:
def cleaner(raw, freqlist):
    """
    iEEG PREPROCESSING PIPELINE
    INPUT: RAW iEEG (MNE)
    OUTPUT: CLEANED iEEG ('picks')
    # note: resampling should already be based on a filtered signal!
    # (i.e., first filtering, then down sampling)
    """
    ### 1. rereference data (average rereference)
    raw.set_eeg_reference('average', projection=True)
    # raw.plot_psd(area_mode='range', tmax=10.0) # visual verification
    print('Original sampling rate:', raw.info['sfreq'], 'Hz')

    ### 2. notch filter
    raw = raw.notch_filter(np.arange(50, int(raw.info['sfreq'] / 2) - 1, 50), filter_length='auto',
                           phase='zero')  # 60, 241, 60
    #add notch filter for variable frquencies - take mean of always two values
    freqlist_notch = []
    for i in range(int(len(freqlist)/2)):
        freqlist_notch.append(statistics.mean([freqlist[i*2],freqlist[i*2+1]]))
    if(len(freqlist_notch)>=1): #check that there is frequency in list
        raw = raw.notch_filter(freqlist_notch, filter_length='auto', phase='zero')
    # raw.plot_psd(tmin=tmin, tmax=tmax, fmin=fmin, fmax=fmax, n_fft=n_fft,
    #              n_jobs=1, proj=True, ax=ax, color=(1, 0, 0), picks=picks) # visual verification

    ### 3. other filters
    # low pass filter (250Hz)
    raw = raw.filter(None, 250. if 250 < (int(raw.info['sfreq'] / 2)-1) else 125., h_trans_bandwidth='auto', filter_length='auto', phase='zero')
    # high pass filter (1Hz) - remove slow drifts
    raw = raw.filter(1., None, l_trans_bandwidth='auto', filter_length='auto', phase='zero')
    # raw.plot_psd(area_mode='range', tmax=10.0) # visual verification

    ### 4. downsampling (200Hz)
    raw = raw.resample(256, npad='auto')
    print('New sampling rate:', raw.info['sfreq'], 'Hz')

    ### 5. reject bad channels
    #def check_bads_adaptive(raw, picks, fun=np.var, thresh=3, max_iter=np.inf):
    #    ch_x = fun(raw[picks, :][0], axis=-1)
    #    my_mask = np.zeros(len(ch_x), dtype=bool)
    #    i_iter = 0
    #    while i_iter < max_iter:
    #        ch_x = np.ma.masked_array(ch_x, my_mask)
    #        this_z = stats.zscore(ch_x)
    #        local_bad = np.abs(this_z) > thresh
    #        my_mask = np.max([my_mask, local_bad], 0)
    #        print('iteration %i : total bads: %i' % (i_iter, sum(my_mask)))
    #        if not np.any(local_bad):
    #            break
    #        i_iter += 1
    #    bad_chs = [raw.ch_names[i] for i in np.where(ch_x.mask)[0]]
    #    return (bad_chs)

    # Find the first index of the super-bad channels
    #'''endIndex = 1
    #for i, name in enumerate(
    #        raw.info['ch_names']):  # can add new logic to reject other channels that are definitely bad
    #    if len(re.compile(r'C\d{3}').findall(name)) > 0:
    #        endIndex = i
    #        break

    #bad_chs = raw.ch_names[endIndex:]
    #bad_chs.extend(check_bads_adaptive(raw, list(range(0, endIndex)), thresh=3))
    #raw.info['bads'] = bad_chs'''
    ##fix from Paul
    #raw.info['bads'] = check_bads_adaptive(raw, list(range(0, len(raw.ch_names))), thresh=3)

    #     print(bad_chs)
    #     print(len(raw.info['bads'])) # check which channels are marked as bad
    ### PICK ONLY GOOD CHANNELS:
    #picks = raw.pick_types(eeg=True, meg=False, exclude='bads')
    #print("NUMBER OF CHANNELS FOR SUBJECT {}: {}".format(subject, len(picks.info['chs'])))
    #     print("THIS SHOULD BE 0: {}".format(len(picks.info['bads'])) ) # check statement


    return raw


# +
#takes in two excels, one containing electrode positions and one containing interictal event patterns
def find_bestchans_positionwise(interictal_event_file, electrode_position_file, maxdist_elec):
    #check for channels having sharp waves
    bestchans_read = pd.read_csv(interictal_event_file)
 #   print(bestchans_read)
 #   bestchans_read = bestchans_read[bestchans_read['pattern'] == "sharp waves"]
    #print(bestchans_read)
    #read in electrode positions
    electrode_positions = pd.read_csv(electrode_position_file)

    #turn wave positions into list of absolute numbers
    sharp_waves = [i for i, e in enumerate(electrode_positions['name']) if e in bestchans_read['electrode'].values.tolist()]

    sharp_wave_coords = electrode_positions.iloc[sharp_waves]
    sharp_wave_coords = sharp_wave_coords.reset_index()

    chan_list = [] #list saving all
    dist_list = [] #only for plotting all distances

    #calculate which electrodes are close to initial "good" electrodes
    for i in range(len(electrode_positions)):
        for j in range(len(sharp_wave_coords)):
            p1 = np.array([electrode_positions['x'][i], electrode_positions['y'][i], electrode_positions['z'][i]])
            p2 = np.array([sharp_wave_coords['x'][j], sharp_wave_coords['y'][j], sharp_wave_coords['z'][j]])
            squared_dist = np.sum((p1-p2)**2, axis=0)
            dist = np.sqrt(squared_dist)
            dist_list.append(dist)
            if(dist < maxdist_elec):
                chan_list.append(i)

    plt.hist(dist_list)

    close_good_chans = electrode_positions['name'][chan_list]

    #check that found channels were initially not considered as bad channels - in segment goodnes excel table
    final_chans = np.intersect1d(data.index.values, close_good_chans.values)

    #create list with absolute positions for further calculation
    bestchans = [i for i, e in enumerate(data.index.values) if e in final_chans]
    print(bestchans)
    return bestchans



#calcualte best channels and near neighbors
#bestchans = find_bestchans_positionwise(interictal_event_file, electrode_position_file, maxdist_electrodeneighbors)
#add neighbors to "good" channels with sharp waves, only considering the order of electrodes on brain
def find_bestchans_numberwise(interictal_event_file):
    #check for channels having sharp waves
    bestchans_read = pd.read_csv(interictal_event_file)
#    bestchans_read = bestchans_read[bestchans_read['pattern'] == "sharp waves"]['electrode']
    bestchans_read= bestchans_read['electrode']
    bestchanlist = []
    for i in bestchans_read:
        bestchanlist.append(i)
        n = i[:len(i)-1] #get the letters of the electrode
        if(i[len(i)-1] == '0'): #if on left outer side, only add right neighbor (no left present)
            bestchanlist.append(n + str(int(i[len(i)-1])+1))
        else:
            if(i[len(i)-1] == '8'): #if on the right outer side, only add left neighbor (no right present)
                bestchanlist.append(n + str(int(i[len(i)-1])-1))
            else: #if in middle: add left and right neighbor
                bestchanlist.append(n + str(int(i[len(i)-1])-1))
                bestchanlist.append(n + str(int(i[len(i)-1])+1))
    return bestchanlist

from scipy.signal import filtfilt
from sklearn import preprocessing
from scipy.signal import hilbert


def calc_syn(chunk, ds):
    win_len = np.shape(chunk)[0]
    
    data_scaler = preprocessing.StandardScaler().fit(chunk.T)
    data_norm = phase(data_scaler.transform(chunk.T)).T
    
    myhilbert =  np.zeros((len(ds),))

    j=0
    for d in ds:
        z = 0
        for acolumn in data_norm:
            total_nodes = int(np.round((d/100)*len(acolumn)))
            z = z + np.mean([np.exp(1j * a_val) for a_val in acolumn[:total_nodes]])

        myhilbert[j] = np.abs(z)/win_len
        j=j+1
    return myhilbert

def phase(a):
    a -= np.mean(a)
    b = hilbert(a)
    b = np.abs(np.arctan2(np.imag(b),np.real(a)))
    return b




bestchans = find_bestchans_numberwise(interictal_event_file)
#print('before')
print(bestchans)
print(len(bestchans))
#print('after')
#fail = superfail

# +
t1=perf_counter()
subject = pat
raw = create_csv_preprocessing(pat, data_file, meta_path, data_path)
if raw is None:
    print('No good IED channels in file. -> Script exited')
    sys.exit()
picks = cleaner(raw, freqlist)

data = pd.DataFrame(picks.get_data().T, columns = picks.ch_names)

### quick check: transpose if not in proper format (rows = chans, cols = timepoints) - build on this later.
#if len(data) > len(data.columns):
#    data = data.T
#    if type(data[0][0]) == str:
#        data = data.drop(data.columns[0], axis=1)
#    data = data.astype(float)
#    print('CHECK: Number of channels ~ %d' % len(data))
#else:
#    data = data.astype(float)

print(f'Data cleaned {perf_counter()-t1:.0f}/{perf_counter()-t0:.0f} sec.')
# -

# #### Clean old data paths
# AUTO DUMP IED IMAGES: clears dir containing spectrograms if produced in previous iteration


def locate_downsample_freq(sample_freq, min_freq=200, max_freq=340):
    min_up_factor = np.inf
    best_candidate_freq = None
    for candidate in range(min_freq, max_freq+1):
        down_samp_rate = sample_freq / float(candidate)
        down_factor, up_factor = down_samp_rate.as_integer_ratio()
        if up_factor <= min_up_factor:
            min_up_factor = up_factor
            best_candidate_freq = candidate
    return best_candidate_freq


def butter_bandpass(low_limit, high_limit, samp_freq, order=5):
    nyquist_limit = samp_freq / 2
    low_prop = low_limit / nyquist_limit
    high_prop = high_limit / nyquist_limit
    b, a = signal.butter(order, [low_prop, high_prop], btype='band')
    def bb_filter(data):
        return signal.filtfilt(b, a, data)
    return bb_filter


def detect(channel, samp_freq, return_eeg=False, temp_func=None, signal_func=None):
    # assume that eeg is [channels x samples]
    # Round samp_freq to the nearest integer if it is large
    if samp_freq > 100:
        samp_freq = int(np.round(samp_freq))
    down_samp_freq = locate_downsample_freq(samp_freq)
    template = signal.triang(np.round(down_samp_freq * 0.06))
    kernel = np.array([-2, -1, 1, 2]) / float(8)
    template = np.convolve(kernel, np.convolve(template, kernel, 'valid') ,'full')
    if temp_func:
        template = temp_func(template, samp_freq)
    if signal_func:
        channel = signal_func(channel, samp_freq)

    down_samp_rate = samp_freq / float(down_samp_freq)
    down_samp_factor, up_samp_factor = down_samp_rate.as_integer_ratio()
    channel = signal.detrend(channel, type='constant')
    results = template_match(channel, template, down_samp_freq)
    up_samp_results = [np.round(spikes * down_samp_factor / float(up_samp_factor)).astype(int) for spikes in results]
    if return_eeg:
        return up_samp_results, [channel[start:end] for start, end in results]
    else:
        return up_samp_results

def template_match(channel, template, down_samp_freq, thresh=7, min_spacing=0): #######@@@############################## CHANGE: d:7,0
    template_len = len(template)
    cross_corr = np.convolve(channel, template, 'valid')
    cross_corr_std = med_std(cross_corr, down_samp_freq)
    detections = []
    # catch empty channels
    if cross_corr_std > 0:
        # normalize the cross-correlation
        cross_corr_norm = ((cross_corr - np.mean(cross_corr)) / cross_corr_std)
        cross_corr_norm[1] = 0
        cross_corr_norm[-1] = 0
        # find regions with high cross-corr
        if np.any(abs(cross_corr_norm > thresh)):
            peaks = detect_peaks(abs(cross_corr_norm), mph=thresh, mpd=template_len)
            peaks += int(np.ceil(template_len / 2.)) # center detection on template
            peaks = [peak for peak in peaks if peak > template_len and peak <= len(channel)-template_len]
            if peaks:
                # find peaks that are at least (min_spacing) secs away
                distant_peaks = np.diff(peaks) > min_spacing * down_samp_freq
                # always keep the first peak
                to_keep = np.insert(distant_peaks, 0, True)
                peaks = [peaks[x] for x in range(len(peaks)) if to_keep[x] == True]
                detections = [(peak-template_len, peak+template_len) for peak in peaks]
    return np.array(detections)

def med_std(signal, window_len):
    window = np.zeros(window_len) + (1 / float(window_len))
    std = np.sqrt(np.median(np.convolve(np.square(signal), window, 'valid') - np.square(np.convolve(signal, window, 'valid'))))
    return std

#electrode_position_file = f'{meta_path}/{pat}_electrodes.csv'
#elecs = pd.read_csv(electrode_position_file, index_col=0)
#mydist = utils.get_channels_dists(elecs)
window = int(2*60*(raw.info['sfreq']))


##### First lets do the conventional bullshit

data = pd.DataFrame(picks.get_data().T, columns = picks.ch_names)
N = np.shape(data)[0]
data['Time'] = np.arange(N) * raw.info['sfreq']



import numpy as np
import multiprocessing

# Define your list of 'bands' values that you want to use
band_ranges = [ (4, 8), (8, 13), (13, 30),(55, 95),(0.1,99)]

# Define a function that calculates band power for a given 'bands' value
def calculate_band_power(args):
    mydata = args[0]
    bands = args[1]
    calc_parameters = {
        'welch_detrend': 'constant',
        'welch_window': 'hann',
        'welch_overlap': 0.5,
        'welch_res': 8,
        'band_power_window_length_in_secs': 0.125,
        'down_sample_to': 256,
        'cut_sym_after_filter_in_sec': 60,
        'log': True,
        'acf_length': 120,
        'acf_overlap': 0,
        'band_power_mode': 'median',
        'notch_filter': [47.5, 52.5, 61, 63, 93, 95, 97.5, 102.5, 104.5, 108.5],
        'patID': 'pat958'
    }
    
    calc_parameters['bands'] = (bands,)  # Set the 'bands' parameter
    band_power_data = utils.get_band_power_segments(mydata, fs=raw.info['sfreq'], norm=False, **calc_parameters)
    
    times = sorted(band_power_data.index.unique())[::int((calc_parameters['acf_length'] * (1 - calc_parameters['acf_overlap'])) / calc_parameters['band_power_window_length_in_secs'])]
    
    # Delete times for which the end of the acf would be outside of the time window
    times = times[:len(times) - int(np.ceil(calc_parameters['acf_overlap']) / (1 - calc_parameters['acf_overlap']))]
    #downsamples = [10, 30, 50, 70, 100]

    all_ind = np.zeros((len(times), np.shape(mydata)[1] - 1))
    all_var = np.zeros((len(times), np.shape(mydata)[1] - 1))
    all_syn = np.zeros((len(times), len(downsamples)))
    all_distavg =  np.zeros((len(times),))
    
    i = 0
    for T in times:
        bool_loc = np.logical_and(band_power_data.index >= T, band_power_data.index < T + calc_parameters['acf_length'])
        data_window = band_power_data.loc[bool_loc]
        data_window.drop(columns='Band', inplace=True)
        data_window.set_index('Time', drop=True, inplace=True)
        acf = utils.estimated_autocorrelation(data_window)
        ind, hm = utils.hwhm(acf, hmfunc=lambda x: utils.hm_from_lag_x_med_3_2(x, x=1))
        all_ind[i, :] = ind
        all_var[i, :] = np.var(data_window.to_numpy(), axis = 0)
        all_syn[i, :] = calc_syn(data_window.to_numpy(), downsamples)
        
        ss = utils.cc_by_binned(data_window, mydist)
        all_distavg[i] = np.mean(ss.iloc[9:79])
        
        
        i += 1
    
    return all_var #, all_syn, all_distavg
# Assuming you have already defined or imported these variables: mydata and raw

# Create a multiprocessing Pool with the number of desired processes
num_processes = len(band_ranges)
pool = multiprocessing.Pool(processes=num_processes)

# Use the Pool to calculate band power for each 'bands' value in parallel
results = pool.map( calculate_band_power, [(data, bands) for bands in band_ranges])

# Close the pool and wait for all processes to finish
pool.close()
pool.join()


out_pathnew = f'/data/gpfs-1/groups/ag_meisel/work/epilepsiae_bids/{pat}/AUTOVARSYNDIST_JAN/'
os.makedirs(out_pathnew, exist_ok=True)


# 'results' now contains the all_ind and all_var arrays for each 'bands' value
# You can access the results as results[0], results[1], results[2] for the corresponding 'bands' values
bands_names = ['theta', 'alpha', 'beta', 'gamma','broad']

for i in range(num_processes):
    bb = bands_names[i]
    variance_save = results[i]
    variance_save = results[i][1]
    syn_save = results[i][2]
    dist_save = results[i][3]
    
    np.savetxt(f'{out_pathnew}/{pat}_{data_f}_{bb}_lag_times_psd.txt', acf_save)
    np.savetxt(f'{out_pathnew}/{pat}_{data_f}_{bb}_variance_psd.txt', variance_save)
    np.savetxt(f'{out_pathnew}/{pat}_{data_f}_{bb}_syn_psd.txt', syn_save)
    np.savetxt(f'{out_pathnew}/{pat}_{data_f}_{bb}_dist_psd.txt', dist_save)
    
print('huzzah')    
#import numpy as np
#import multiprocessing


downsamples = [10, 30, 50, 70, 100]

# Define frequency bands
freq_bands = {
    'theta': (4, 8),
    'alpha': (8, 13),
    'beta': (13, 30),
    'gamma': (55, 95),
    'broad': (0.1, 99)
}
bands_names = ['theta', 'alpha','beta','gamma', 'broad']
window = int(2*60*(raw.info['sfreq']))
#j =0
# Bandpass filter the data for each frequency band
for band, (low, high) in freq_bands.items():
    mydata = picks.copy().filter(low, high, verbose='info')
    datapd = pd.DataFrame(mydata.get_data().T, columns = mydata.ch_names)
    N = np.shape(datapd)[0]
    datapd['Time'] = np.arange(N) * picks.info['sfreq']
    
    mytimes = np.arange(int(N/window))*window
    myvars =  np.zeros((len(mytimes),np.shape(data)[1] -1))
    myauto = np.zeros((len(mytimes),np.shape(data)[1] -1))
    mydistavg = np.zeros((len(mytimes),))
    mysyn = np.zeros((len(mytimes), len(downsamples)))

    
    i=0
    for T in mytimes:
        toanalyze = datapd.iloc[T:T+window]
        toanalyze.set_index('Time', drop=True, inplace=True)
        
        ss = utils.cc_by_binned(toanalyze, mydist)
        mydistavg[i] = np.mean(ss.iloc[9:79])


        myvars[i,:] = np.var(toanalyze.to_numpy(), axis =0)
        acf_est = utils.estimated_autocorrelation(toanalyze)
        ind, hm = utils.hwhm(acf_est, hmfunc=lambda x:utils.hm_from_lag_x_med_3_2(x, x=1))
        myauto[i,:] = (ind/200)*(0.125)
        
        mysyn[i,:] = calc_syn(toanalyze.to_numpy(), downsamples)

        i = i +1

        
for bb in bands_names:
    np.savetxt(f'{out_pathnew}/{pat}_{data_f}_{bb}_variance.txt', myvars)
    np.savetxt(f'{out_pathnew}/{pat}_{data_f}_{bb}_lag_times.txt', myauto)
    np.savetxt(f'{out_pathnew}/{pat}_{data_f}_{bb}_dist.txt', mydistavg)
    np.savetxt(f'{out_pathnew}/{pat}_{data_f}_{bb}_syn.txt', mysyn)


    
    j=j+1
